const express = require('express');
const app = express();
app.use(express.json());
app.get("/", (req,res)=>{
    res.send("Hello World");
})
app.get("/api",(req,res)=>{
    res.json({success:true,message:"Hello World"})
})
const port = 4000;
app.listen(port,()=>console.log(`listen on port ${port}`));